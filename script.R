
library(readr)
library(ggplot2)
library(magrittr)
library(dplyr)
library(tidyr)
library(stringr)
library(ggpol)
library(gganimate)

df = read_csv('data/brasil.csv')
df_aux = read_csv2('data/df_aux.csv') %>% mutate_all(as.factor)

df %>% glimpse

# Data Wrangling
df1 = df %>%
  select(ano, starts_with(c(
    'populacao_homens_', 'populacao_mulheres_'
  ))) %>%
  na.omit %>% pivot_longer(cols = !ano, values_to = 'pop') %>%
  mutate(
    faixa_etaria = as.factor(str_extract(name,"[0-9,]+")),
    genero = as.factor(
      ifelse(str_detect(name, '_homens_'),
             'Masculino', 'Feminino')),
      genero = factor(genero, levels = c('Masculino', 'Feminino')),
      ano = as.integer(ano),
      pop = ifelse(genero == 'Masculino', as.integer(pop * -1),
                   as.integer(pop))
    ) %>% left_join(df_aux, by = c('faixa_etaria' = 'id')) %>% 
  mutate(range = paste0(range,' anos') %>% as.factor()) %>% 
  select(-name,-faixa_etaria)

# Definindo estilo geral do gráfico
my_chart = function () {
  theme(
    plot.background = element_blank(),
    axis.title.y = element_blank(),
    legend.title = element_blank(),
    panel.background = element_blank(),
    panel.border = element_blank(),
    strip.background = element_blank(),
    strip.text.x = element_blank(),
    panel.grid.minor = element_blank(),
    panel.grid.major = element_blank(), 
    axis.text = element_text(size = 14),
    legend.key.size = unit(0.75, 'cm'),
    legend.text = element_text(
      size = 15,
      face = 'bold'
    ),
    plot.title = element_text(
      size = 22,
      hjust = 0.5,
      face = 'bold'
    ),
    plot.subtitle = element_text(
      size = 14,
      hjust = 0.5,
      face = 'bold'
    ),
    axis.title.x = element_text(
      size = 16,
      face = 'bold'
    ),
    plot.caption = element_text(
      size = 9,
      hjust = 0.5,
      face = 'italic',
      color = 'gray'
    )
  )
}

gg = df1 %>% 
  ggplot(aes(x = range,
             y = pop / 1000,
             fill = genero)) +
  geom_bar(stat =  'identity') +
  scale_fill_manual(
    values = c('#4682b4', '#ee7989')) +
  coord_flip() +
  facet_share(
    ~ genero,
    dir =  'h',
    scales =  'free',
    reverse_num = TRUE
  ) + my_chart() +
  #gganimate
  transition_states(
    ano,
    transition_length = 1,
    state_length = 1
  ) + 
  enter_fade() +
  exit_fade() + 
  ease_aes('cubic-in-out') +
  labs(
    title = 'A pirâmide etária brasileira, 1991 - 2010\n\n Ano: {closest_state}',
    subtitle = '\n\nGrupos de idade',
    y = '\n\nPopulação (em milhares)',
    caption = '\n\nFonte: Base dos Dados / Atlas Brasil'
    # caption = '\n\nFonte: https://basedosdados.org/dataset'
  )

animate(
  gg,
  fps = 24,
  duration = 30,
  width = 500,
  height = 500,
  renderer = gifski_renderer('plot1.gif')
)
